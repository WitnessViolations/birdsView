# Readme for birdsView


# Architecture
This Sofware is made up of different apps

## Collect

## Preserve

## Process

## Verify

## Investigate

# Inspirations
- [hkmap.live](https://hkmap.live/#)
- [Example Spotting](https://hkmap.live/m/3925189940)
- [Cop Map](https://www.cop-map.com/)

# Install Python and Docker
Install the Dependencies
```shell
sudo apt install python3-pip docker.io
```

Create a virtual env for this project
```shell
python3 -m venv env
```

Create your Environment Variables
```shell
cp .env.example .env
```
Set the correct password and user names
and then source the .env variables and the env file for python

```shell
source .env
source env/bin/activate
```

Then setup a virtual env for pip in the folder env
```shell
pip3 install -r requirements.txt
```

Setup a the Database with a volume which is persisted
```shell
 docker volume create pg_data
 docker run --name=postgis -d -e POSTGRES_USER=$PG_USER -e POSTGRES_PASS=$PG_PASS -e POSTGRES_DBNAME=$PG_NAME \
  -e ALLOW_IP_RANGE=0.0.0.0/0 -p 5432:5432 -v pg_data:/var/lib/postgresql --restart=always kartoza/postgis:9.6-2.4 
```

Change the directory to the Backend
```shell
cd birdsView
```

Next step is to create the database migration and then run the migrations
```shell
 python manage.py makemigrations 
 python manage.py migrate
```

Now create a administrator for the website and provide a secure password
```shell
python manage.py createsuperuser  --email admin@example.com --username admin
```

Now you can start the server
```shell
 python manage.py runserver
 python manage.py runserver_plus
```

To group all the application and output into PNG file
```shell
python manage.py graph_models vista -o imagefile_name.png
```

These are the URLs which currenty work:
- (http://127.0.0.1:8000/vista/map/)
- (http://127.0.0.1:8000/vista/)

Here is our API
- (http://127.0.0.1:8000/markers)

Here is the Backend for the Admin
- (http://127.0.0.1:8000/admin)

Here is the Admin Panel for the Telegram Bots
- (http://localhost:8000/admin/django-telegrambot/)

# Guides
## Backend
- [Tutorial for REST with leaflet and geo django](ttps://www.paulox.net/2019/07/25/web-maps-the-mer-et-demeures-project/#web-maps-the-mer-et-demeures-project)

## Frontend
- [Bootstrap Work completed](https://dev.to/thalesbruno/django-bootstrap-basic-setup-5dmb)
- [Django Bootstrap](https://django-bootstrap.readthedocs.io/en/latest/)
- [Vue Django Templates](https://github.com/gtalarico/django-vue-template/blob/master/backend/urls.py)
- [Django and Vue.js](https://auth0.com/blog/building-modern-applications-with-django-and-vuejs/)

## Telegram
- [Django-Telegrambot Documentation](https://django-telegram-bot.readthedocs.io/en/latest/readme.html#quickstart)

# Interviews
## Honk Kong
- [Interview with Kamu Administrator of hkmap.live](https://unchainedpodcast.com/an-interview-with-the-anonymous-developer-of-hkmap-live/)