import Vue from 'vue'
import Vuex from 'vuex'
import { LMap, LTileLayer, LMarker, LIcon, LPopup } from "vue2-leaflet";
import { latLng, icon } from "leaflet";
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userValidated: false,
    points: [

    ],
    incidents: [

    ]
  },
  mutations: {
    setPoint(state, payload) {
      state.points.push(payload);
    },
    setIncident(state, payload) {
      state.incidents.push(payload);
    },
    removedPoint(state, payload) {
      state.points = payload;
    },
  },
  actions: {
    async cleanPoints({ commit }) {
      commit('removedPoint', [])
    },

    async addPoint({ commit }, point) {
      commit('setPoint', point)
    },

    async loadCivilian({ commit },) {

      console.log('Civilian')
      try {

        const res = await fetch(`https://comap.live/api/participant/`, {
          method: 'GET',
          headers: {
            'content-type': 'application/json',
          },
        })
        var dataDBF = (await res.json())
        var dataDB = dataDBF.results.features
        dataDB.forEach(element => {

          if (element.geometry !== null) {
            var pointToadd = {
              marker: L.latLng(element.geometry.coordinates[1], element.geometry.coordinates[0]),
              icon: icon({
                iconUrl: require("leaflet/dist/images/marker-icon-2x.png"),
                iconUrl: require("leaflet/dist/images/marker-icon.png"),
                shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
                iconSize: [25, 32],
                iconAnchor: [20, 20],
              }),
              name: element.properties.first_name,
              lastName: element.properties.last_name,
              title: element.properties.situation,
              description: element.properties.description,
              showParagraph: true,
            };

            commit('setPoint', pointToadd)
          }


        });


      }
      catch (error) {
        console.log(error)
      }
    },
    async loadPolice({ commit },) {

      console.log('Police')
      try {

        const res = await fetch(`https://comap.live/api/officer/`, {
          method: 'GET',
          headers: {
            'content-type': 'application/json',
          },
        })
        var dataDBF = (await res.json())
        var dataDB = dataDBF.results.features


        dataDB.forEach(element => {
          if (element.geometry !== null) {
            var pointToadd = {
              marker: L.latLng(element.geometry.coordinates[1], element.geometry.coordinates[0]),
              icon: icon({
                iconUrl: require("leaflet/dist/images/marker-icon-2x.png"),
                iconUrl: require("leaflet/dist/images/marker-icon.png"),
                shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
                iconSize: [25, 32],
                iconAnchor: [20, 20],
              }),
              name: element.properties.first_name,
              lastName: element.properties.last_name,
              title: element.properties.situation,
              description: element.properties.description,
              showParagraph: true,
            };

            commit('setPoint', pointToadd)
          }


        });

      }
      catch (error) {
        console.log(error)
      }

    },
    async loadIncident({ commit },) {

      console.log('Incident')
      try {

        const res = await fetch(`https://comap.live/api/incidents/`, {
          method: 'GET',
          headers: {
            'content-type': 'application/json',
          },
        })
        var dataDBF = (await res.json())
        var dataDB = dataDBF.results.features

        console.log(dataDB)
        dataDB.forEach(element => {
          if (element.geometry !== null) {
            var incidentToadd = {
              marker: L.latLng(element.geometry.coordinates[1], element.geometry.coordinates[0]),
              icon: icon({
                iconUrl: require("leaflet/dist/images/marker-icon-2x.png"),
                iconUrl: require("leaflet/dist/images/marker-icon.png"),
                shadowUrl: require("leaflet/dist/images/marker-shadow.png"),
                iconSize: [25, 32],
                iconAnchor: [20, 20],
              }),
              time: element.properties.time,
              title: element.properties.name,
              official_deaths: element.properties.official_deaths,
              shots_fired_on_persons: element.properties.shots_fired_on_persons,
              showParagraph: true,
            };

            commit('setIncident', incidentToadd)
          }


        });

      }
      catch (error) {
        console.log(error)
      }

    },

  },
  modules: {
  }
})
