"""Markers urls."""

from rest_framework.routers import DefaultRouter

from .views import *

router = DefaultRouter()
router.register(r"incidents", IncidentViewSet, basename="incident")
router.register(r"media", EyewitnessReportViewSet, basename="media")
router.register(r"proof", ProofViewSet, basename="proof")
router.register(r"denial", DenialViewSet, basename="denial")
router.register(r"officer", OfficerViewSet, basename="officer")
router.register(r"participant", ParticipantViewSet, basename="civilian")
urlpatterns = router.urls
