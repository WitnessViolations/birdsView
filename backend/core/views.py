"""Markers view."""

from rest_framework.mixins import CreateModelMixin, ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import (
    IsAuthenticatedOrReadOnly,
    DjangoModelPermissionsOrAnonReadOnly,
    IsAuthenticated,
    AllowAny,
)
from rest_framework.viewsets import GenericViewSet, ModelViewSet, ReadOnlyModelViewSet
from rest_framework_gis.filters import InBBoxFilter

from collect.models import Participant, EyewitnessReport
from contacts.models import Area
from core.serializers import (
    AreaSerializer,
    IncidentSerializer,
    EyewitnessReportSerializer,
    ProofSerializer,
    DenialSerializer,
    ParticipantSerializer,
    OfficerSerializer,
)
from officers.models import Officer
from process.models import Incident
from verify.models import Proof, Denial


class IncidentViewSet(ModelViewSet):
    bbox_filter_field = "location"
    filter_backends = (InBBoxFilter,)
    queryset = Incident.objects.filter()
    serializer_class = IncidentSerializer

    permission_classes = (
        IsAuthenticatedOrReadOnly,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class EyewitnessReportViewSet(CreateModelMixin, GenericViewSet):
    bbox_filter_field = "where"
    filter_backends = (InBBoxFilter,)
    queryset = EyewitnessReport.objects.filter()
    serializer_class = EyewitnessReportSerializer

    permission_classes = (
        AllowAny,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class ProofViewSet(CreateModelMixin, GenericViewSet):
    queryset = Proof.objects.filter()
    serializer_class = ProofSerializer
    permission_classes = (
        IsAuthenticated,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class DenialViewSet(CreateModelMixin, GenericViewSet):
    queryset = Denial.objects.filter()
    serializer_class = DenialSerializer
    permission_classes = (
        IsAuthenticated,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class OfficerViewSet(
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet
):
    bbox_filter_field = "last_known_location"
    filter_backends = (InBBoxFilter,)
    queryset = Officer.objects.filter()
    serializer_class = OfficerSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class ParticipantViewSet(
    CreateModelMixin, ListModelMixin, RetrieveModelMixin, GenericViewSet
):
    bbox_filter_field = "last_known_location"
    filter_backends = (InBBoxFilter,)
    queryset = Participant.objects.filter()
    serializer_class = ParticipantSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        DjangoModelPermissionsOrAnonReadOnly,
    )


class AreaViewSet(ReadOnlyModelViewSet):
    bbox_filter_field = "borders"
    filter_backends = (InBBoxFilter,)
    queryset = Area.objects.filter()
    serializer_class = AreaSerializer
    permission_classes = (
        IsAuthenticatedOrReadOnly,
        DjangoModelPermissionsOrAnonReadOnly,
    )
