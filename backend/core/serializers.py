from rest_framework.serializers import ModelSerializer
from rest_framework_gis.serializers import GeoFeatureModelSerializer

from collect.models import EyewitnessReport, Participant
from contacts.models import Area
from officers.models import Officer
from process.models import Incident
from verify.models import Proof, Denial


class AreaSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Area
        geo_field = "borders"
        fields = ("name",)


class IncidentSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Incident
        geo_field = "location"
        fields = (
            "name",
            "tag",
            "time",
            "location",
            "official_deaths",
            "counted_deaths",
            "shots_fired",
            "description",
            "meta_description",
            "image",
        )


class EyewitnessReportSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = EyewitnessReport
        geo_field = "location"
        fields = (
            "name",
            "time",
            "restrictions",
            "notarisation",
            "image",
            "movie",
            "url",
            "comment",
        )


class ProofSerializer(ModelSerializer):
    class Meta:
        model = Proof
        fields = (
            "created",
            "comment",
            "reporter",
        )


class DenialSerializer(ModelSerializer):
    class Meta:
        model = Denial
        fields = (
            "created",
            "comment",
            "reporter",
        )


class ParticipantSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Participant
        geo_field = "last_known_location"
        fields = (
            "situation",
            "description",
            "meta_description",
            "first_name",
            "last_name",
            "description",
        )


class OfficerSerializer(GeoFeatureModelSerializer):
    class Meta:
        model = Officer
        geo_field = "last_known_location"
        fields = (
            "unit",
            "first_name",
            "last_name",
            "description",
            "meta_description",
            "appears_in_media",
        )
