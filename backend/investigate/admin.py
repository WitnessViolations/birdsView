from django.contrib import admin
# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin
from import_export import resources
from import_export.admin import ImportExportMixin

from investigate.models import Document, Case, Court


class CaseResource(resources.ModelResource):
    class Meta:
        model = Case


class DocumentResource(resources.ModelResource):
    class Meta:
        models = Document


class DocumentAdmin(ImportExportMixin, OSMGeoAdmin):
    search_fields = ["name"]
    list_display = (
        "name",
        "file",
        "incident",
    )
    resource_class = DocumentResource
    fields = ("name", ("file", "incident"))


admin.site.register(Document, DocumentAdmin)


class CaseAdmin(ImportExportMixin, OSMGeoAdmin):
    search_fields = ["name"]
    list_display = ("name",)
    resource_class = CaseResource


admin.site.register(Case, CaseAdmin)


@admin.register(Court)
class CourtAdmin(OSMGeoAdmin):
    pass