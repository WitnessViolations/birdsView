from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class InvestigateConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "investigate"
    verbose_name = _("Legal Investigation")
