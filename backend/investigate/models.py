from django.contrib.gis.db import models
# Create your models here.
from django.utils.translation import gettext_lazy as _

from process.models import Incident


class Document(models.Model):
    name = models.CharField(
        max_length=64, help_text=_("Name of the document"), verbose_name=_("name")
    )
    file = models.FileField(
        help_text=_("Please the file of the document"), verbose_name=_("file")
    )
    incident = models.ForeignKey(
        Incident,
        on_delete=models.PROTECT,
        related_name="document",
        related_query_name="documents",
        help_text=_("Incident to which this documents corresponds"),
        verbose_name=_("incident"),
    )

    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")

    def __str__(self):
        return "%s %s" % (self.id, self.name)


    class Meta:
        verbose_name = _("Document")
        verbose_name_plural = _("Documents")



class Court(models.Model):
    name = models.CharField(
        max_length=128, help_text=_("Name of the court"), verbose_name=_("name")
    )


    def __str__(self):
        return "%s %s" % (self.id, self.name)

    class Meta:
        verbose_name = _("Court")
        verbose_name_plural = _("Courts")



class Case(models.Model):
    number = models.PositiveIntegerField(
        verbose_name=_("number"), help_text=_("Case number"), null=True
    )
    name = models.CharField(
        max_length=128, help_text=_("Name of the court case"), verbose_name=_("name")
    )

    court = models.ForeignKey(
        Court,
        on_delete=models.PROTECT,
        related_name="cases",
        verbose_name=_("court"),
        help_text=_("Court for this case"),
        null=True,
    )

    doc_url = models.URLField(
        help_text=_("URL for this case"), verbose_name=_("url"), blank=True
    )
    incident = models.ForeignKey(
        Incident,
        on_delete=models.CASCADE,
        related_name="cases",
        help_text=_("About which incident is this case?"),
        verbose_name=_("incident"),
    )
    plaintiff = models.CharField(
        max_length=128,
        blank=True,
        verbose_name=_("plaintiff"),
        help_text=_("Name of the plaintiff"),
    )
    defendant = models.CharField(
        max_length=128,
        blank=True,
        verbose_name=_("defendant"),
        help_text=_("Name of the defendant"),
    )

    class Meta:
        verbose_name = _("Case")
        verbose_name_plural = _("Cases")

    def __str__(self):
        return "%s %s" % (self.id, self.name)


class Tags(models.TextChoices):
    ACCESS_TO_INFORMATION = "A", "Access to Information"
