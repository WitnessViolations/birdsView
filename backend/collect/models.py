# Create your models here.
from django.contrib.gis.db import models
from django.utils.translation import gettext_lazy as _

from contacts.models import Profile
from process.models import Incident, MediaRestrictions


class Gender(models.Model):
    name = models.CharField(
        max_length=64,
        help_text=_("What is the name of the gender?"),
        verbose_name=_("name"),
    )

    class Meta:
        verbose_name = _("Gender")
        verbose_name_plural = _("Genders")

    def __str__(self):
        return self.name


class Race(models.Model):
    name = models.CharField(
        max_length=64,
        help_text=_("What is the name of this race?"),
        verbose_name=_("name"),
    )

    class Meta:
        verbose_name = _("Race")
        verbose_name_plural = _("Races")

    def __str__(self):
        return self.name


class Situation(models.TextChoices):
    UNIDENTIFIED = "U", _("Unidentified Person")
    RAPED = "R", _("Sexually Violated")
    MISSING = "I", _("Missing")
    DISPLACEMENT = "F", _("Forced displacement")
    MURDER = "M", _("Murdered")
    ALIVE = "A", _("Found alive")
    DEAD = "D", _("Found dead")
    DAMAGED = "P", _("Suffered permanent damages")


class ParticipantRestrictions(models.TextChoices):
    NO_SHARING = "n", _("No, don’t share contact info")
    SUBJECT_SHARING = "s", _("Yes, can share my info with other impacted subjects")
    PRESS_SHARING = "p", _("Yes, can share my info with members of the press")
    BOOTH_SHARING = "b", _("Ok to share contact with both")
    ASK_FIRST = "a", _("Contact participant before sharing any info")
    NO_CONTACT = "c", _("Do not contact")


class AbstractPerson(models.Model):
    description = models.TextField(
        max_length=512,
        help_text=_("Describe the person"),
        verbose_name=_("description"),
    )
    # TODO narrative
    meta_description = models.CharField(
        max_length=140,
        blank=True,
        help_text=_("Short description of the person"),
        verbose_name=_("meta description"),
    )
    first_name = models.CharField(
        max_length=50,
        blank=True,
        help_text=_("First name of the Person"),
        verbose_name=_("first name"),
    )
    last_name = models.CharField(
        max_length=50,
        blank=True,
        help_text=_("Last name of the Person"),
        verbose_name=_("last name"),
    )
    appears_in_media = models.ManyToManyField(
        "collect.EyewitnessReport",
        blank=True,
        help_text=_("In which media does this person appear?"),
        verbose_name=_("appears in media"),
    )
    last_known_location = models.PointField(
        blank=True,
        null=True,
        help_text=_("What is the last known location of this person?"),
        verbose_name=_("last known location"),
    )
    image = models.ImageField(
        blank=True, help_text=_("Portrait image"), verbose_name=_("image")
    )
    gender = models.ManyToManyField(
        Gender,
        help_text=_("Please select your appropriate gender"),
        blank=True,
        verbose_name=_("gender"),
    )
    race = models.ManyToManyField(
        Race,
        help_text=_("Please select the race of the participant"),
        blank=True,
        verbose_name=_("race"),
    )

    @property
    def full_name(self):
        "Returns the person's full name."
        return "%s %s" % (self.first_name, self.last_name)

    def __str__(self):
        return "%s %s %s" % (
            self.id,
            self.first_name,
            self.last_name,
        )  # ,self.last_seen)

    class Meta:
        verbose_name = _("Abstract person")
        verbose_name_plural = _("Abstract persons")
        abstract: True
        ordering = ["last_name"]


class Participant(AbstractPerson):
    situation = models.CharField(
        max_length=1,
        choices=Situation.choices,
        default=Situation.MISSING,
        help_text=_("What kind of situation?"),
        verbose_name=_("situation"),
    )
    confidentiality = models.CharField(
        max_length=1,
        choices=ParticipantRestrictions.choices,
        default=ParticipantRestrictions.ASK_FIRST,
        help_text=_("Please tell us who we can contact"),
        verbose_name=_("confidentiality"),
    )

    class Meta:
        verbose_name = _("Participant")
        verbose_name_plural = _("Participants")


class EyewitnessReport(models.Model):
    name = models.CharField(
        max_length=140,
        help_text=_("What is the name of this Report?"),
        verbose_name=_("name"),
    )
    restrictions = models.CharField(
        max_length=1,
        choices=MediaRestrictions.choices,
        default=MediaRestrictions.DISCRETION,
        help_text=_("What restrictions apply to this media?"),
        verbose_name=_("restrictions"),
    )
    timestamp = models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        help_text=_("When was this submited?"),
        verbose_name=_("timestamp"),
    )
    time = models.DateTimeField(
        help_text=_("When did this happen?"), verbose_name=_("time"), null=True
    )
    notarisation = models.CharField(
        max_length=128,
        blank=True,
        help_text=_("If you have cameraV please add the notarisation of the image"),
        verbose_name=_("notarisation"),
    )
    url = models.URLField(
        blank=True, help_text=_("Where did you find this Media?"), verbose_name=_("url")
    )
    comment = models.TextField(
        max_length=1024,
        help_text=_("Please explain in detail what happened"),
        verbose_name=_("comment"),
    )
    location = models.PointField(
        blank=True,
        null=True,
        help_text=_("Where did this happen?"),
        verbose_name=_("location"),
    )
    incident = models.ForeignKey(
        Incident,
        on_delete=models.PROTECT,
        related_name="report",
        related_query_name="reports",
        help_text=_("To which incident does this media belong?"),
        verbose_name=_("incident"),
        blank=True,
        null=True,
    )
    reporter = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        related_name="medias",
        help_text=_("Which reporter has submited this"),
        verbose_name=_("reporter"),
    )

    def __str__(self):
        return "%s: %s" % (self.id, self.comment)

    class Meta:
        verbose_name = _("Report")
        verbose_name_plural = _("Reports")
        ordering = ["timestamp"]
