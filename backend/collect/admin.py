from django.contrib import admin
from django.contrib.admin import TabularInline
# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib.gis.db import models
from django.forms import TextInput, Textarea
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportMixin

from birdsView.settings.base import LAT, LON, ZOOM
from collect.models import Participant, EyewitnessReport
from preserve.admin import (
    VideoSourceInline,
    InternetSourceInline,
    ImageSourceInline,
    LivestreamSourceInline,
)
from process.admin import (
    ParticipantAtIncidentInline,
)
from verify.models import Proof, Denial


class ParticipantResource(resources.ModelResource):
    class Meta:
        model = Participant


class ProofInline(TabularInline):
    formfield_overrides = {
        models.CharField: {"widget": TextInput(attrs={"size": "20"})},
        models.TextField: {"widget": Textarea(attrs={"rows": 1, "cols": 20})},
    }
    model = Proof


class DenialInline(TabularInline):
    formfield_overrides = {
        models.CharField: {"widget": TextInput(attrs={"size": "20"})},
        models.TextField: {"widget": Textarea(attrs={"rows": 1, "cols": 20})},
    }
    model = Denial


class ParticipantAdmin(ImportExportMixin, OSMGeoAdmin):
    search_fields = ["full_name"]
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "id",
        "situation",
        "first_name",
        "last_name",
        "meta_description",
        "image",
    )
    resource_class = ParticipantResource
    list_filter = ("situation",)
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "situation",
                    ("first_name", "last_name"),
                    "description",
                    "last_known_location",
                )
            },
        ),
        (
            _("Research"),
            {"fields": ("image", "gender", "appears_in_media", "meta_description")},
        ),
    )
    inlines = [ProofInline, DenialInline, ParticipantAtIncidentInline]


admin.site.register(Participant, ParticipantAdmin)


class EyewitnessReportResource(resources.ModelResource):
    class Meta:
        model = EyewitnessReport


class EyewitnessReportAdmin(OSMGeoAdmin):
    search_fields = ["comment", "name", "url"]
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "name",
        "time",
        "restrictions",
        "timestamp",
        "comment",
    )
    resource_class = EyewitnessReportResource
    list_filter = ("restrictions",)
    fieldsets = (
        (
            _("Media"),
            {
                "fields": (
                    ("name", "restrictions"),
                    "comment",
                    "time",
                    "reporter",
                    "url",
                )
            },
        ),
        (_("Research"), {"fields": ("incident", "location", "notarisation")}),
    )
    inlines = [
        ImageSourceInline,
        VideoSourceInline,
        InternetSourceInline,
        LivestreamSourceInline,
    ]
    # inlines = [ProofInline, DenialInline] FIXME manyToMany no foreign key


admin.site.register(EyewitnessReport, EyewitnessReportAdmin)

# FIXME no null