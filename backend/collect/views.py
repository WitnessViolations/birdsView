import json

from django.core.serializers import serialize
# Create your views here.
from django.views.generic import TemplateView

from process.models import Incident


class MarkersMapView(TemplateView):
    """Markers map view."""

    template_name = "collect/map.html"

    def get_context_data(self, **kwargs):
        """Return the view context data."""
        context = super().get_context_data(**kwargs)
        context["markers"] = json.loads(serialize("geojson", Incident.objects.all()))
        # context["missing"] = json.loads(serialize("geojson", Participant.objects.all()))
        return context


class OfficerView(TemplateView):

    template_name = "core/index.html"
