const attribution =
  '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
const map = L.map("map");
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution: attribution,
}).addTo(map);
const incidents = JSON.parse(
  document.getElementById("incidents-data").textContent
);
let feature = L.geoJSON(incidents, {
  onEachFeature: function (feature, layer) {
    layer.bindPopup(
      "<h1>" +
        feature.properties.name +
        "</h1> <img src=" +
        feature.properties.img +
        "/><p>Time: " +
        feature.properties.time +
        ", " +
        feature.properties.meta_description +
        "</p><p>Official deaths: " +
        feature.properties.official_deaths +
        ", Official deaths: " +
        feature.properties.counted_deaths +
        "</p>"
    );
  },
}).addTo(map);
map.fitBounds(feature.getBounds(), { padding: [100, 100] });
