import React from 'react'
import {
    Map,
    TileLayer,
    GeoJSOn
} from 'react - leaflet'

export default class Map extends Component {
    state = {
        geoJson: {}
    }

    onMove = () => {
        fetch('/markers')
            .then(geoJson => this.setState({
                geoJson
            }))
        }
        render() {
    return (
        <Map center= {c} zoom= {z} onMoveend= {this.onMove}>
        <TileLayer url="//{s}.tile.osm.org/ {z}/{x}/{y}.png"/>
        <GeoJSON data= {this.state.geoJson} />
        </Map>
    )
}

    }
