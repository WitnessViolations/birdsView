from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class CollectConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "collect"
    verbose_name = _("Collect Information")
