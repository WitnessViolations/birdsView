from django.urls import path

from collect.views import MarkersMapView, OfficerView

app_name = "collect"

urlpatterns = [
    path("", MarkersMapView.as_view()),
    path("officers/", OfficerView.as_view()),
]
