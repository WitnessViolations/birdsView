from django.contrib import admin
# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin

from birdsView.settings.base import LAT, LON, ZOOM
from verify.models import Proof, Denial


@admin.register(Proof)
class ProofAdmin(OSMGeoAdmin):
    search_fields = ["comment"]
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "created",
        "comment",
        # "media",
        "reporter",
    )
    # inlines = [ImageSourceInline, VideoSourceInline, InternetSourceInline, LivestreamSource]


@admin.register(Denial)
class DenialAdmin(OSMGeoAdmin):
    search_fields = ["comment"]
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "created",
        "comment",
        # "media",
        "reporter",
    )
    # inlines =  [ImageSourceInline, VideoSourceInline, InternetSourceInline, LivestreamSource]
