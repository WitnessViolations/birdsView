from django.db import models
# Create your models here.
from django.utils.translation import gettext_lazy as _

from collect.models import Participant
from contacts.models import Profile
from officers.models import Officer, OfficerHistory, Badge
from preserve.models import AbstractSource
from process.models import Incident, ParticipantAtIncident, OfficerAtIncident


class AbstractValidation(models.Model):
    created = models.DateTimeField(
        auto_now_add=True, db_index=True, verbose_name=_("created")
    )
    reporter = models.ForeignKey(
        Profile,
        on_delete=models.CASCADE,
        related_name="validations",
        help_text=_("Who reported this information?"),
        verbose_name=_("reporter"),
    )
    comment = models.TextField(
        max_length=512, help_text=_("Please comment here"), verbose_name=_("comment")
    )
    participant = models.ForeignKey(
        Participant,
        related_name="validated_participants",
        on_delete=models.CASCADE,
        help_text=_("Which participants do you want to validate?"),
        verbose_name=_("incident"),
        null=True,
    )
    incident = models.ForeignKey(
        Incident,
        related_name="validated_incidents",
        on_delete=models.CASCADE,
        help_text=_("Which incident do you want to validate?"),
        verbose_name=_("incident"),
        null=True,
    )
    participant_at_incident = models.ForeignKey(
        ParticipantAtIncident,
        related_name="validated_participant_at_incidents",
        on_delete=models.CASCADE,
        help_text=_("Was this participant was at this incident?"),
        verbose_name=_("participant at incident"),
        null=True,
    )
    officer_at_incident = models.ForeignKey(
        OfficerAtIncident,
        related_name="validated_officer_at_incidents",
        on_delete=models.CASCADE,
        help_text=_("Was this officer at this incident?"),
        verbose_name=_("participant at incident"),
        null=True,
    )
    officer = models.ForeignKey(
        Officer,
        related_name="validated_officers",
        on_delete=models.CASCADE,
        help_text=_("Validate details about this officer"),
        verbose_name=_("officer history"),
        null=True,
    )
    officer_history = models.ForeignKey(
        OfficerHistory,
        related_name="validated_officer_histories",
        on_delete=models.CASCADE,
        help_text=_("Select a history of a officer you want to validate"),
        verbose_name=_("officer history"),
        null=True,
    )
    officer_badge = models.ForeignKey(
        Badge,
        related_name="validated_officer_badges",
        on_delete=models.CASCADE,
        help_text=_("Select a badge of a officer you want to validate"),
        verbose_name=_("officer badge"),
        null=True,
    )
    source = models.ForeignKey(
        AbstractSource,
        related_name="validated_sources",
        on_delete=models.CASCADE,
        help_text=_("Select a source you want to verify"),
        verbose_name=_("source"),
        null=True,
    )

    def __str__(self):
        return "%s %s (%s)" % (self.comment, self.reporter, self.created)

    class Meta:
        verbose_name = _("abstract validation")
        verbose_name_plural = _("abstract validations")
        ordering = ["created"]
        abstract: True


class Proof(AbstractValidation):
    pass

    class Meta:
        verbose_name = _("Proof")
        verbose_name_plural = _("Proofs")


class Denial(AbstractValidation):
    pass

    class Meta:
        verbose_name = _("Denial")
        verbose_name_plural = _("Denials")
