echo "Writing Database model into documentation"
python manage.py graph_models -a -o ../doc/source/_static/img/model.png
python manage.py graph_models -g -a -o ../doc/source/_static/img/model_group.png
python manage.py graph_models -a -X AbstractValidation,Denial,Proof -o ../doc/source/_static/img/model_no_validation.png
python manage.py graph_models -a -I AbstractValidation,Denial,Proof -o ../doc/source/_static/img/model_validation.png
echo "done"