docker stop postgis && docker rm postgis && docker volume rm pg_data && docker volume create pg_data &&\
  docker run --name=postgis -d -e POSTGRES_USER=$PG_USER -e POSTGRES_PASS=$PG_PASS -e POSTGRES_DBNAME=$PG_NAME \
  -e ALLOW_IP_RANGE=0.0.0.0/0 -p 5432:5432 -v pg_data:/var/lib/postgresql --restart=always kartoza/postgis:9.6-2.4  && sleep 5 && \
  python manage.py makemigrations && python manage.py migrate && \
  python manage.py createsuperuser  --email admin@example.com --username admin && \
  python manage.py runserver
