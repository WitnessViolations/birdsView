from django.contrib.gis.db import models
# Create your models here.
from django.utils.translation import gettext_lazy as _

from collect.models import AbstractPerson


class OfficerType(models.TextChoices):
    CAR = "C", _("Police Car")
    CIV = "Z", _("Participant Police")
    SQUAD = "P", _("Police Squad")
    MILITARY = "M", _("Military Squad")
    TEARGAS = "T", _("Tear Gas")
    DEMONSTRATOR = "D", _("Demonstrator")


class OfficerRank(models.TextChoices):
    OFFICER = "o", _("Officer")


class Officer(AbstractPerson):
    unit = models.CharField(
        max_length=1,
        choices=OfficerType.choices,
        default=OfficerType.CAR,
        help_text=_("In which unit is the officer?"),
        verbose_name=_("unit"),
    )
    open_over_sight_link = models.URLField(
        blank=True,
        help_text=_("Enter the open Oversight url here"),
        verbose_name=_("open oversight url"),
    )

    # todo fix later
    # current_squad = models.ForeignKey(
    #     "AbstractUnit",
    #     on_delete=models.SET_NULL,
    #     null=True,
    #     related_name="officers",
    #     related_query_name="officer",
    #     blank=True,
    #     help_text=_(""
    # )

    class Meta:
        verbose_name = _("Officer")
        verbose_name_plural = _("Officers")


class OfficerHistory(models.Model):
    officer = models.ForeignKey(
        Officer,
        on_delete=models.CASCADE,
        related_name="histories",
        help_text=_("Which officer?"),
        verbose_name=_("officer"),
    )
    start_date = models.DateField(
        help_text=_("When did the officer start?"),
        verbose_name=_("start date"),
    )
    end_date = models.DateField(
        help_text=_("When did this officer end?"),
        verbose_name=_("end date"),
    )
    # FIXME todo event type
    # todo event detail
    link_url = models.URLField(
        help_text=_(
            "The URL or link to an external webpage that contains information about the case or complaint."
        ),
        verbose_name=_("link url"),
    )

    def __str__(self):
        return "%s %s %s" % (
            self.officer,
            self.start_date,
            self.end_date,
        )

    class Meta:
        verbose_name = _("History")
        verbose_name_plural = _("Histories")


class Department(models.Model):
    name = models.CharField(
        max_length=64,
        help_text=_("Official Name of the Department"),
        verbose_name=_("name"),
    )

    def __str__(self):
        return self.name  # ,self.last_seen)

    class Meta:
        ordering = ["name"]
        verbose_name = _("Department")
        verbose_name_plural = _("Departments")


class OfficerDepartment(models.Model):
    officer = models.ForeignKey(
        Officer,
        on_delete=models.CASCADE,
        related_name="departments",
        help_text=_("Officer"),
        verbose_name=_("officer"),
    )
    department = models.ForeignKey(
        "Department",
        on_delete=models.CASCADE,
        related_name="officers",
        help_text=_("Department of the Officer"),
        verbose_name=_("department"),
    )
    start_date = models.DateField(
        help_text=_("Start date in this department"), verbose_name=_("start date")
    )
    end_date = models.DateField(
        blank=True,
        help_text=_("End date in this department"),
        verbose_name=_("end date"),
    )

    def __str__(self):
        return "%s %s %s" % (
            self.id,
            self.officer,
            self.department,
        )

    class Meta:
        verbose_name = _("Officer Department")
        verbose_name_plural = _("Officer Departments")


class Badge(models.Model):
    number = models.CharField(
        max_length=64,
        help_text=_("Badge number of the Officer"),
        verbose_name=_("number"),
    )
    officer = models.ForeignKey(
        "Officer",
        on_delete=models.CASCADE,
        related_name="badges",
        related_query_name="badge",
        help_text=_("To which officer does this badge belong"),
        verbose_name=_("officer"),
    )
    rank = models.CharField(
        max_length=1,
        choices=OfficerRank.choices,
        default=OfficerRank.OFFICER,
        help_text=_("What rank does this officer have on his badge?"),
        verbose_name=_("rank"),
    )
    start_date = models.DateField(
        help_text=_("When did the officer start wearing this badge?"),
        verbose_name=_("start date"),
    )
    end_date = models.DateField(
        help_text=_("When did the officer stop wearing this badge?"),
        verbose_name=_("end date"),
    )

    def __str__(self):
        return "%s %s" % (
            self.officer,
            self.number,
        )  # FIXME add is current

    class Meta:
        verbose_name = _("badge")
        verbose_name_plural = _("badges")

    # todo ocalculate property is_current = models.BooleanField()
