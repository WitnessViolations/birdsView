from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class OfficersConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "officers"
    verbose_name = _("Badges & History of Officers")
