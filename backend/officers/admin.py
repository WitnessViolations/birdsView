from django.contrib import admin
from django.contrib.admin import StackedInline
# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportMixin

from birdsView.settings.base import LAT, LON, ZOOM
from officers.models import (
    Department,
    OfficerHistory,
    OfficerDepartment,
    Badge,
    Officer,
)
from process.admin import OfficerAtIncidentInline


class OfficerDepartmentInline(StackedInline):
    model = OfficerDepartment


class OfficerHistoryInline(StackedInline):
    model = OfficerHistory


class OfficerBadgeInline(StackedInline):
    model = Badge


@admin.register(Department)
class DepartmentAdmin(OSMGeoAdmin):
    list_display = ("name",)
    inlines = [OfficerDepartmentInline]


@admin.register(OfficerHistory)
class OfficerHistoryAdmin(OSMGeoAdmin):
    list_display = (
        "start_date",
        "end_date",
        "link_url",
    )


# @admin.register(OfficerPhoto)
# class OfficerPhotoAdmin(OSMGeoAdmin):
#     list_display = (
#         "photo",
#         "caption",
#     )
#
#
# @admin.register(OfficerDepartment)
# class OfficerDepartmentAdmin(OSMGeoAdmin):
#     list_display = (
#         "start_date",
#         "end_date",
#     )


@admin.register(Badge)
class BadgeAdmin(OSMGeoAdmin):
    list_display = (
        "number",
        "officer",
        "start_date",
        "end_date",
        "rank",
    )


class OfficerResource(resources.ModelResource):
    class Meta:
        model = Officer


class OfficerAdmin(ImportExportMixin, OSMGeoAdmin):
    search_fields = ["full_name"]
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "id",
        "unit",
        "first_name",
        "last_name",
        "meta_description",
        "description",
        "last_known_location",
        "image",
    )
    fieldsets = (
        (
            None,
            {
                "fields": (
                    "unit",
                    "first_name",
                    "last_name",
                    "description",
                    "last_known_location",
                )
            },
        ),
        (
            _("Public Info"),
            {"fields": ("meta_description", "image", "gender", "appears_in_media")},
        ),
    )
    inlines = [
        OfficerBadgeInline,
        OfficerDepartmentInline,
        OfficerHistoryInline,
        OfficerAtIncidentInline,
    ]


admin.site.register(Officer, OfficerAdmin)
