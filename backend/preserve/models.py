from django.db import models
# Create your models here.
from django.utils.translation import gettext_lazy as _

from collect.models import EyewitnessReport


class AbstractSource(models.Model):
    report = models.ForeignKey(
        EyewitnessReport,
        on_delete=models.PROTECT,
        help_text=_("For which report is this source?"),
        verbose_name=_("report"),
        null=True,  # FIXME
    )
    description = models.TextField(
        max_length=2048,
        help_text=_("Please describe what you have witnessed"),
        verbose_name=_("description"),
        blank=True,
    )

    class Meta:
        verbose_name = _("Abstract Source")
        verbose_name_plural = _("Abstract sources")
        abstract: True


class AbstractMediaSource(AbstractSource):
    date_recorded = models.DateField(
        verbose_name=_("date"), help_text=_("Date of publication")
    )
    media_creator = models.CharField(
        blank=True,
        verbose_name=_("media creator"),
        help_text=_("Who is the creator?"),
        max_length=128,
    )  # TODO trusted Sources
    media_source = models.CharField(
        blank=True,
        verbose_name=_("media source"),
        help_text=_("Who is the source?"),
        max_length=128,
    )  # TODO trusted sources
    original_media_format = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_("original media format"),
        help_text=_("What was the original media format?"),
    )
    original_media_id = models.CharField(
        max_length=64,
        blank=True,
        verbose_name=_("original media id"),
        help_text=_("Original ID of this media Source"),
    )

    class Meta:
        verbose_name = _("Abstract Media Source")
        verbose_name_plural = _("Abstract Media Sources")


class ImageSource(AbstractMediaSource):
    image = models.ImageField(
        help_text=_("Please upload the image here"), verbose_name=_("image")
    )

    class Meta:
        verbose_name = _("Image Source")
        verbose_name_plural = _("Image sources")


class VideoSource(AbstractMediaSource):
    video = models.FileField(
        help_text=_("Please upload the video here"), verbose_name=_("movie")
    )

    class Meta:
        verbose_name = _("Video Source")
        verbose_name_plural = _("Video sources")


class InternetSource(AbstractSource):
    url = models.URLField(
        help_text=_("Please add internet source"), verbose_name=_("source")
    )

    class Meta:
        verbose_name = _("Internet Source")
        verbose_name_plural = _("Internet sources")


class LivestreamSource(AbstractSource):
    stream_url = models.URLField(
        help_text=_("Please the live stream url"), verbose_name=_("source")
    )

    class Meta:
        verbose_name = _("Livestream Source")
        verbose_name_plural = _("Livestream sources")


class DocumentSource(AbstractSource):
    title = models.CharField(
        max_length=128, verbose_name=_("title"), help_text=_("Title of the Document")
    )
    publication_date = models.DateField(
        verbose_name=_("publication date"),
        help_text=_("Date of publication"),
        null=True,
    )
    author = models.CharField(
        blank=True,
        help_text=_("Author of this Document"),
        verbose_name=_("author"),
        max_length=128,
    )
    full_text = models.TextField(
        blank=True,
        verbose_name=_("full text"),
        help_text=_("Paste full text here"),
        max_length=64 * 1024,
    )
    file = models.FileField(
        blank=True, help_text=_("Please upload your file here"), verbose_name=_("file")
    )

    class Meta:
        verbose_name = _("Document Source")
        verbose_name_plural = _("Document Sources")
