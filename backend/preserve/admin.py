# Register your models here.
from django.contrib.admin import StackedInline

from preserve.models import *


class ImageSourceInline(StackedInline):
    model = ImageSource


class VideoSourceInline(StackedInline):
    model = VideoSource


class InternetSourceInline(StackedInline):
    model = InternetSource


class LivestreamSourceInline(StackedInline):
    model = LivestreamSource


class DocumentSourceInline(StackedInline):
    model = DocumentSource

# FIXME Admin for all Sources