from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

class PreserveConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "preserve"
    verbose_name = _("Preserve Information")
