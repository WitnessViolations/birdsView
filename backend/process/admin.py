from django.contrib import admin
from django.contrib.admin import TabularInline, StackedInline
# Register your models here.
from django.contrib.gis.admin import OSMGeoAdmin
from django.contrib.gis.db import models
from django.forms import TextInput, Textarea
from django.utils.translation import gettext_lazy as _
from import_export import resources
from import_export.admin import ImportExportMixin

from birdsView.settings.base import LAT, LON, ZOOM
from investigate.models import Case, Document
from process.models import ParticipantAtIncident, OfficerAtIncident, Incident


class ParticipantAtIncidentInline(StackedInline):
    model = ParticipantAtIncident


class OfficerAtIncidentInline(TabularInline):
    model = OfficerAtIncident


@admin.register(OfficerAtIncident)
class OfficerAtIncidentAdmin(OSMGeoAdmin):
    pass


@admin.register(ParticipantAtIncident)
class ParticipantAtIncidentAdmin(OSMGeoAdmin):
    pass


class DocumentInline(TabularInline):
    model = Document


class CaseInline(TabularInline):
    model = Case


class IncidentResource(resources.ModelResource):
    class Meta:
        model = Incident


class IncidentAdmin(ImportExportMixin, OSMGeoAdmin):
    search_fields = ["name", "desctiption"]
    formfield_overrides = {
        models.CharField: {"widget": TextInput(attrs={"size": "20"})},
        models.TextField: {"widget": Textarea(attrs={"rows": 4, "cols": 40})},
    }
    default_lat = LAT
    default_lon = LON
    default_zoom = ZOOM
    list_display = (
        "name",
        "id",
        "tag",
        "time",
        "meta_description",
        "use_of_force",
    )
    resource_class = IncidentResource
    list_filter = (
        "tag",
        "time",
    )

    fieldsets = (
        (None, {"fields": ("name", "tag", "testimony", "time", "location")}),
        (
            _("Research"),
            {
                "fields": (
                    ("official_deaths", "counted_deaths"),
                    "result_of_stop",
                    "use_of_force",
                    "shots_fired",
                )
            },
        ),
        (_("Publishing"), {"fields": ("description", ("meta_description", "image"))}),
    )
    inlines = [
        OfficerAtIncidentInline,
        ParticipantAtIncidentInline,
        CaseInline,
        DocumentInline,
    ]


admin.site.register(Incident, IncidentAdmin)
