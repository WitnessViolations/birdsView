# Generated by Django 3.2.3 on 2021-06-29 18:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('process', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='officeratincident',
            name='role',
            field=models.CharField(choices=[('P', 'Patrol Officer'), ('S', 'Supervising Officer')], default='P', help_text='What was the role of the officer?', max_length=1, verbose_name='role'),
        ),
        migrations.AddField(
            model_name='officeratincident',
            name='unit',
            field=models.CharField(choices=[('BP', 'Bicycle Patrol')], default='BP', help_text='Department unit that this officer was serving in the capacity of at the time of this incident.', max_length=2, verbose_name='unit'),
        ),
    ]
