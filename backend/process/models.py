from django.contrib.gis.db import models

# Create your models here.
from django.utils.translation import gettext_lazy as _

# from officers.models import Officer


class Status(models.TextChoices):
    MURDER = "MU", _("Murder")
    MASSACRE = "MA", _("Massacre")
    KIDNAPPING = "KI", _("Kidnapping")
    EX_VIOLENCE = "EV", _("Excessive physical violence")
    PSY_VIOLENCE = "PV", _("Psychological violence")
    NEG_VIOLENCE = "NV", _("Neglectful violence")
    BEATING = "BE", _("Beating")
    CHOKEHOLDS = "CH", _("Chokeholds")
    FIREARMS = "FI", _("Use of firearms")
    DISPLACEMENT = "FD", _("Forced displacement")
    TAKEDOWN = "TD", _("Unlawful takedown")
    PRISONER_ABUSE = "PA", _("Prisoner Abuse")
    TASER = "TA", _("Unwarranted use of tasers")
    DISCRIMINATION = "DI", _("Discrimination")
    FALSE_ARREST = "FA", _("False arrest")
    WRONG_IMPRISONMENT = "WI", _("Wrongful imprisonment")
    SEXUAL_ASSAULT = "SA", _("Sexual assault")
    SEXUAL_ABUSE = "SB", _("Sexual abuse")
    MEDICAL_DENIAL = "MD", _("Denial of medical care")
    STALKING = "ST", _("Stalking")
    BRIBING = "BR", _("Bribing")
    OF_DUTY = "OD", _("Off-duty misconduct")
    UNDER_INFLUENCE = "UI", _("Influence of drugs or alcohol while on duty")


class Testimony(models.TextChoices):
    IMMEDIATE_COPWATCHER = "i", _("Copwatcher eyewitness account (immediate)")
    MEMORY_COPWATCHER = "m", _("copwatcher eyewitness account (memory)")
    IMMEDIATE_OTHER = "o", _("Other eyewitness account (immediate)")
    MEMORY_OTHER = "n", _("Other eyewitness account (memory)")
    DOCUMENTATION = "d", _("Non-eyewitness, based on documentation")
    BACKGROUND_INFORMATION = "b", _("Non-eyewitness, additional background information")


class MediaRestrictions(models.TextChoices):
    DISCRETION = "d", _("Use Curator Discretion")
    PUBLIC = "p", _("OK to Make Footage Public")
    CONCEAL = "o", _("Blur Faces / Obscure Identities")
    CONFIDENTIAL = "c", _("Do Not Release")


class ParticipantRoleAtIncident(models.TextChoices):
    SUBJECT = (
        "s",
        _(
            "A person who is the focus of the incident that the record documents. Can include victims, survivors, people stopped or detained. Unidentified subjects should be described in the narrative"
        ),
    )
    COPWATCHER = (
        "w",
        _(
            "An observer trained by or affiliated with a copwatching organization, and who is practicing copwatching principles of non-violence and non-interference."
        ),
    )
    LEGAL_OBSERVER = (
        "o",
        _(
            "A person specifically trained to gather facts at the scene of an incident/protest/strike"
        ),
    )
    MEDIA = (
        "m",
        _(
            "A person acting in their capacity as a journalist, either with press credentials or who self-identifies as a member of the press"
        ),
    )
    CLOSE_TO_SUBJECT = (
        "t",
        _(
            "A person connected with the subject of the incident, such as a family member, friend, partner, etc"
        ),
    )
    COMPLAINANT = (
        "c",
        _(
            "A person alleging police misconduct or crime happened and is pursuing further action"
        ),
    )


class OfficerRoleAtIncident(models.TextChoices):
    PATRROL_OFFICER = "P", _("Patrol Officer")
    SUPERVISOR = "S", _("Supervising Officer")


class OfficerUnitAtIncident(models.TextChoices):

    BicyclePatrol = "BP", _("Bicycle Patrol")  # FIXME Add 93


class ParticipantRole(models.TextChoices):
    DEMONSTRATOR = "D", _("Demonstrator")


class StopType(models.TextChoices):
    CONSENSUAL_ENCOUNTER = "C", _("Consensual Stop or Encounter")


class Incident(models.Model):
    # TODO privacy FIXME Tag is the internal code
    tag = models.CharField(
        max_length=2,
        choices=Status.choices,
        default=Status.MURDER,
        help_text=_("What kind of incident is this?"),
        verbose_name=_("tag"),
    )
    testimony = models.CharField(
        max_length=2,
        choices=Testimony.choices,
        default=Testimony.MEMORY_OTHER,
        help_text=_("From whom does this testimony come from?"),
        verbose_name=_("testimony"),
    )

    name = models.CharField(
        max_length=64,
        help_text=_("Please name the incident"),
        verbose_name=_("name"),
    )
    result_of_stop = models.BooleanField(
        help_text=_("Did this happen because of a stop?"),
        verbose_name=_("result of stop"),
    )
    use_of_force = models.BooleanField(
        help_text=_("Was there violence?"), verbose_name=_("use of force")
    )
    time = models.DateTimeField(
        help_text=_(
            "When did this happen? If you don't know the time select a  daytime from the dropdown menu"
        ),
        verbose_name=_("time"),
    )
    location = models.PointField(
        help_text=_("Where did this happen?"),
        verbose_name=_("location"),
    )
    official_deaths = models.PositiveIntegerField(
        blank=True,
        default=0,
        help_text=_("Official death count of civilians as reported"),
        verbose_name=_("official deaths"),
    )
    counted_deaths = models.PositiveIntegerField(
        blank=True,
        default=0,
        help_text=_("How many deaths of civilians were counted"),
        verbose_name=_("counted deaths"),
    )
    shots_fired = models.PositiveIntegerField(
        blank=True,
        default=0,
        help_text=_("How many shots were fired on civilians?"),
        verbose_name=_("shots fired"),
    )
    description = models.TextField(
        max_length=512,
        blank=True,
        help_text=_("Please describe what you have seen"),
        verbose_name=_("description"),
    )
    meta_description = models.CharField(
        max_length=140,
        blank=True,
        help_text=_("Short description"),
        verbose_name=_("meta description"),
    )
    image = models.ImageField(
        blank=True,
        null=True,
        help_text=_(
            "Cover image which is shown on the map. Do not uploaded images here which you want to share"
        ),
        verbose_name=_("image"),
    )

    @property
    def popupContent(self):
        return '<p {}" /><p><{}</p>'.format(self.tag, self.time)

    class Meta:
        verbose_name = _("Incident")
        verbose_name_plural = _("Incidents")
        ordering = ["time"]

    def __str__(self):
        return "%s %s %s" % (self.id, self.tag, self.name)


class ParticipantAtIncident(models.Model):
    incident = models.ForeignKey(
        "Incident",
        on_delete=models.CASCADE,
        related_name="incidents_of_participants",
        help_text=_("At which incident?"),
        verbose_name=_("incident"),
    )
    participant = models.ForeignKey(
        "collect.Participant",
        on_delete=models.CASCADE,
        related_name="participants_at_incident",
        help_text=_("Who was there?"),
        verbose_name=_("participant"),
    )
    role = models.CharField(
        max_length=1,
        choices=ParticipantRoleAtIncident.choices,
        default=ParticipantRoleAtIncident.COMPLAINANT,
        help_text=_("What was the role of the participant?"),
        verbose_name=_("role"),
    )
    injuries = models.TextField(
        max_length=2 * 1024,
        help_text=_(
            "Any injuries to the participant caused by officer actions during the incident"
        ),
        verbose_name=_("injuries"),
        blank=True,
    )
    confidentiality = models.CharField(
        max_length=1,
        choices=MediaRestrictions.choices,
        default=MediaRestrictions.CONCEAL,
        help_text=_("Please tell us what we can do with this information"),
        verbose_name=_("confidentiality"),
    )

    def __str__(self):
        return "%s %s %s %s" % (
            self.participant,
            self.incident,
            self.confidentiality,
            self.role,
        )

    # FIXME add stop type and person type

    class Meta:
        verbose_name = _("Participant at Incident")
        verbose_name_plural = _("Participant at Incidents")


class OfficerAtIncident(models.Model):
    officer = models.ForeignKey(
        "officers.Officer",
        on_delete=models.CASCADE,
        related_name="incidents",
        help_text=_("Which officer was at the incident?"),
        verbose_name=_("officer"),
    )
    incident = models.ForeignKey(
        Incident,
        on_delete=models.CASCADE,
        related_name="officers",
        help_text=_("Which incident was this?"),
        verbose_name=_("incident"),
    )
    role = models.CharField(
        max_length=1,
        choices=OfficerRoleAtIncident.choices,
        default=OfficerRoleAtIncident.PATRROL_OFFICER,
        help_text=_("What was the role of the officer?"),
        verbose_name=_("role"),
    )
    unit = models.CharField(
        max_length=2,
        choices=OfficerUnitAtIncident.choices,
        default=OfficerUnitAtIncident.BicyclePatrol,
        help_text=_(
            "Department unit that this officer was serving in the capacity of at the time of this incident."
        ),
        verbose_name=_("unit"),
    )

    def __str__(self):
        return "%s %s" % (
            self.officer,
            self.incident,
        )

    class Meta:
        verbose_name = _("Officer at Incident")
        verbose_name_plural = _("Officer at Incidents")
