# Create your models here.
from django.contrib.auth.models import User
from django.contrib.gis.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils.translation import gettext_lazy as _


class ReporterRole(models.TextChoices):
    JOURNALIST = "J", "Journalist"
    MEDIA_GROUP = "M", "Media group"
    HUMANITARIAN_GROUP = "H", "Humanitarian group"
    CITIZEN_REPORTER = "C", "Citizen reporter"
    LAWYER = "L", "Lawyer"


class Profile(models.Model):
    role = models.CharField(
        max_length=1,
        choices=ReporterRole.choices,
        default=ReporterRole.CITIZEN_REPORTER,
        help_text=_("Please select the role of this profile"),
        verbose_name=_("role"),
    )
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        help_text=_("What is the role of this profile"),
        verbose_name=_("user"),
    )
    location = models.CharField(
        help_text=_("Please add the city you are in"),
        blank=True,
        max_length=64,
        verbose_name=_("location"),
    )
    registration_date = models.DateTimeField(
        auto_now_add=True,
        db_index=True,
        verbose_name=_("registration date"),
        help_text=_("When was this profile registered?"),
    )
    nick = models.CharField(
        max_length=64,
        help_text=_("How should this reporter be named?"),
        verbose_name=_("nick"),
    )
    website = models.URLField(
        blank=True,
        help_text=_("Where can I find this reporter in the Internet?"),
        verbose_name=_("website"),
    )
    refeer = models.ForeignKey(
        "self",
        related_name="profile",
        blank=True,
        null=True,
        on_delete=models.CASCADE,
        help_text=_("Who invited this profile?"),
        verbose_name=_("refeer"),
    )

    def __str__(self):
        return self.user.username

    class Meta:
        verbose_name = _("profile")
        verbose_name_plural = _("profiles")


@receiver(post_save, sender=User)
def create_or_update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()


class Area(models.Model):
    name = models.CharField(
        max_length=64,
        help_text=_("How would you like to name this Area?"),
        verbose_name=_("name"),
    )
    borders = models.MultiPolygonField(
        help_text=_("What are the Borders of this Area?"), verbose_name=_("borders")
    )

    def __str__(self):
        return "%s " % (self.name)  # ,self.last_seen)

    class Meta:
        verbose_name = _("Area")
        verbose_name_plural = _("Areas")
        ordering = ["name"]
