from .base import *
from os import environ

# SECURITY WARNING: don't run with debug turned on in production!

DEBUG = True

ALLOWED_HOSTS = ["comap.live", "www.comap.live"]

STATIC_ROOT = environ["STATIC_ROOT"]
MEDIA_ROOT = environ["MEDIA_ROOT"]
SECURE_HSTS_SECONDS = 31536000
SECURE_SSL_REDIRECT = True
SESSION_COOKIE_SECURE = True
CSRF_COOKIE_SECURE = True
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
