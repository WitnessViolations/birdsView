from django.conf.urls import url
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
from django.urls import include, path
from django.utils.translation import gettext_lazy as _
# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularSwaggerView,
    SpectacularRedocView,
)

admin.site.index_title = _("Collect Human Rights Violations")
admin.site.site_header = _("Report & Verify")
admin.site.site_title = _("Site Management")

urlpatterns = i18n_patterns(
    url(r"^i18n/", include("django.conf.urls.i18n")),
    path('admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
    path('secret/', admin.site.urls),
    path("secret/doc/", include("django.contrib.admindocs.urls")),
    path("", include("collect.urls")),
    path("api/", include("core.urls")),
    # url(r"^", include("django_telegrambot.urls")),
    path("api/schema/", SpectacularAPIView.as_view(), name="schema"),
    # Optional UI:
    path(
        "api/schema/swagger-ui/",
        SpectacularSwaggerView.as_view(url_name="schema"),
        name="swagger-ui",
    ),
    path(
        "api/schema/redoc/",
        SpectacularRedocView.as_view(url_name="schema"),
        name="redoc",
    ),
    prefix_default_language=False,
)
