var participants = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-73.65614810111128, 3.940146813358289],
      },

      properties: {
        situation: "I",
        description:
          "Killed in broad daylight by a police officer who was there",
        meta_description: "Killed by Police",
        first_name: "Emanuel",
        last_name: "Hernandez",
      },
    },
  ],
};

var officers = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-75.65614810111128, 3.940146813358289],
      },
      properties: {
        unit: "P",
        first_name: "Fernandez",
        last_name: "Hugo",
        description:
          "Police in plain clothes shots at civilians with an automatic rifle",
        meta_description: "Plain clothes with automatic rifle",
        appears_in_media: [1],
      },
    },
  ],
};

var incidents = {
  type: "FeatureCollection",
  features: [
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-76.63708447343161, 1.153486526483104],
      },
      properties: {
        tag: "MA",
        time: "1991-01-23T06:00:00+01:00",
        name: "MASACRE DE MOCOA",
        official_deaths: 0,
        counted_deaths: 5,
        shots_fired_on_persons: 0,
        description:
          "En la mañana del 23 de enero de 1991, un grupo de 20 miembros de la Policía Nacional llegó a la vereda Villanueva, del municipio de Mocoa, Putumayo, en busca de guerrilleros que días antes habían hecho un atentado en la capital del departamento. Cuando entraron al pueblo se presentó un enfrentamiento con algunos insurgentes, luego del cual los policías capturaron a un guerrillero, detuvieron arbitrariamente a cinco civiles y se los llevaron a todos a la finca Las Palmeras para asesinarlos.",
        meta_description:
          "- Fuerza pública - Mocoa , Putumayo ( Vereda y corregimiento: Villanueva)",
        img: null,
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-75.78369676012166, 6.335679188675046],
      },
      properties: {
        tag: "MU",
        time: "2021-06-10T10:18:21+02:00",
        name: "Military force used against Civilians",
        official_deaths: 6,
        counted_deaths: 9,
        shots_fired_on_persons: 0,
        description: "Shots fired on civilians  by a hairy guy",
        meta_description: "",
        img: null,
      },
    },
    {
      type: "Feature",
      geometry: {
        type: "Point",
        coordinates: [-73.97419541044452, 4.728571784377502],
      },
      properties: {
        tag: "PA",
        time: "2021-06-16T00:00:00+02:00",
        name: "Abuso de un prisionero",
        official_deaths: 0,
        counted_deaths: 0,
        shots_fired_on_persons: 0,
        description: "un caso de abuso policial en la calera",
        meta_description: "un caso de abuso policial en la calera",
        img: null,
      },
    },
  ],
};
