# Goals and Showcase of our Platform

### Kollektiv Stop the Violence

### May 25, 2021

Here you can find our Showcase of the Platform which we are Developing. Please bear with
us, while we work on our online presence this week.

## 1 Objectives

In this Section we will explain the objectives of our Platform threw examples.

### 1.1 General Objective

The main Objective is to protect the lives of those who report and suffer rights violations
through the collection, verification, preservation and investigation of digital information
thus enabling us to document human rights violations.

We our mission is to provide potential evidence for prosecution of human rights violators.
Our accessible Platform will pave the way for legal case against the perpetrators but also
enable journalists and scientist to inform the world about the atrocities and violations of
human rights.


### 1.2 Specific Objectives

These goals reflect our mission. By achieving our these Objectives we will have made a
positive impact in the world. For this to happen we need to:

Spread the Tools Only promoting the use of digital tools for the denunciation and pro-
tection of human rights can we reach a critical mass reporters and people who do not
look the other way. Our Campaign for spreading our tools needs to be online and
offline, so that it reaches a broad mass of people throughout all social classes.

No loss of EvidenceOnly Archiving at-risk digital information, which is valuable to
journalists and human rights defenders, can we mitigate the risk of losing evidence.

Consensus threw rigorous Validation Our goal is to create a verified and searchable
database of human rights violations. By presenting our findings on our websites we
hope that the media and public will join us and voice their disgust against these
immoral deeds.

Safety threw InformationWe hope to prevent any abuse or violation of human rights
by preventing censorship of human rights violations.

Transparent ReportingBy providing curated reports, sources and media for journalists
and human right groups we hope, that the flow of information will be unstoppable.
We want to partner with local social movements and institutions so that they can
supports us with proofs and analysis of what is happening in the conflict area. This
will enable us to give journalists and scientist reports and data with diverse sources,
validations and proofs. What we share and publish can inform the public and help
build cases against the perpetrators of human violence.

TransformationOur first step is to support Colombia’s social transformation by promot-
ing a more equitable system for all. But we do not want to stop here. If we succeed,
we want to provide our knowledge, tools and platform for the whole world.

## 2 Presentation of our Platform

### 2.1 Motivation

It isurgentto shield those who are dying at the hands of the state. Especially at this
time, thus we need to stop this massacre. If we do not want to forget the dead, we need
to commemorate them. For this we need a Platform that offers security to the researchers
and safety for the data which we collect and publish. This is why we have worked on a
safety and privacy concept which protects the identity of the denounces and reporters of
human violence.

Every day and every hour material is uploaded to the internet, which helps to preserve
memories of these violations. Only with this material can we document human rights
violations and to contribute to the truth and reconciliation efforts in conflict zones. Often


this material is removed or deleted from social networks on the grounds that this content
violates the platforms’ terms and conditions, but this is often not the case, and even content
deemed inappropriate by a platform can be of extreme value to the human rights community
due to its significance. This is why we are looking for partners around the world, which
will help us store and preserve this knowledge in a fail safe and redundant way.

It is important to create clear workflows and methodologies, as well as definitions to effec-
tively utilize our tools and support the work of human rights institutions. By agreeing on
common definitions we can try to move threw the barrier of different spoken and written
languages, while trying to be as inclusive as possible to all humans around the earth.

Only threw collaboration in research can occur across all specialties and institutions and
only then is it possible to generate differentiated results which are produced critically and
with high quality. This is due to the fact that if this collaboration does not occur, much of
the data is lost and never used again. Only when everybody participates in our research
can be sure, that we do not culturally approiate or misinterpret what happend around the
world.

Our work and our guides will be free to use and published opens source. This will enable
achieve transparency in our Platform and allows us to develop workflows and methods which
can be adopted by everybody else as seen fit. By publishing in free and open formats we
allow for greater flexibility, scalability and customization. This enables bigger participation
of all communities thus enabling the our project to gain a community offer time.

### 2.2 Short Presentation of tools which we have developed for the moder-

### ation Team

We are developing multiple platforms, but have decided to showcase a platform where people
from social institutions and movements whom we trust can moderate and administrate the
map. This map is accessible for everybody on the Internet.

We provide some Screenshots of our software in order to explain the Features
of our Platform.

2.2.1 Missing Person

The team which is moderating the uploaded data should be able to identify lost people.
This is why we provide a way share Information about lost people. We provide the Option
to specify where or at which Event the person was last.

2.2.2 Incident of Human Right Violations

Here we can administer the Reports of Human Right Violations. We call these incidents.
In the following text we will explain the different Fields. We provide different types of
Incidents such as Events or Protests. These options can help us identify different type of
events, thus allowing us to alert the correct working group. Date and timestamps but also


```
Figure 1: Report of a missing person
```
Figure 2: Here we can add Incidents or Spotting of Human Right Violations


the locations help us track the violence and human rights violations over the course of time.
The description field shows the user on the Map what happened and the corresponding
sources.

2.2.3 Public Map from our Website

We provide a way for users to validate data reported on our platform, this will help us
combat trolls and fake news. We are developing a an anonymous way to submit this data
over bots in messengers such as Telegram, Matrix and others. Our main goal here is to
have an pseudononmyous chain of trust, which will help us find trolls quicker.

The public website will feature a map, accessible by any one. We hope that journalists and
scientiest can use this data.

```
Figure 3: Example Presentation of our Web Map
```
This is how we manage Spottings of Human Right Violations We provide different types
of Events or Spottings which can help us identify different events. Time, location and
date help us track the violence and human rights violations over the course of time. The
description field shows the user on the Map what happend and the corresponding sources.

We provide a way for Users to Validate Data reported on our Platform, this will help us
combat trolls and fake news.We are developing a an anonymous way to submit this data
over bots in messengers such as Telegram, Matrix and others. Our main goal here is to
have an pseudononmyous chain of trust, which will help us find trolls quicker.

The public website will feature a map, accessible by any one. We hope that journalists and
scientist can use this data


References:

- Cop Map- Report cops in your area (https://www.cop-map.com/)
- CamaraV (https://guardianproject.info/archive/camerav/)
- mnemonic (https://mnemonic.org/en/contact-us)
- WITNESS (https://es.witness.org/quienes-somos/)



